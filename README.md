ics-ans-role-localusers
=======================

Ansible role to create local users and groups.

Requirements
------------

- ansible >= 2.7
- molecule >= 2.19

Role Variables
--------------

```yaml
localusers_default_shell: /bin/bash
localusers_create_homedirs: yes
# Create groups for users with the same name as the users group even if
# principal groups is specified
localusers_per_user_groups: no

localusers_default_password: '!!'

# Lists of users to create or delete
localusers:
- name: user1
  comment: User n1
  password: '$6$hE1TkH.X$F9flUhXGiL8zI4lsdsdsdFxN8L8HrMbuP4ZFVbv5MnyeQFAhIAsbTmT6t7.p93FgyiJo3U/aJeiGHzCTUvA.s.'
  group: grp1
  groups:
    - grp2
- name: user2
  groups:
    - grp2
- name: user3
  password: '$6$QhdNz4DlCbtbfUiX$wJIffkZEIHNVcmgQgfUk0sgON/HIXBxzE1qE4qS6IpxaHUP34hWoTreoMxjF1BmpDMDFspegqcYaqa5zQDUwI1'
  uid: 34333
- name: badUser
  state: absent

localusers_groups:
- name: grp1
  gid: 3345
- name: grp2
  gid: 3346
- name: badGrp
  state: absent

localusers_ssh_keys:
- name: user1
  keys:
    - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDcoWQneQ/fbUmZPnTajzAvXC1XOvhD5+61ZPW8obIZ54OpLcNKyOf4TfceapSAFnKidXv+Z9CZ6foXr2/64bgL43ILvSNUhU2ic4Tzy5ARYJ5o84R61La8/nv5eCQTlgQRTqdW8HVR+xFGNUhG44FqHa+yUXpyadKwIVFYuWQAFZ6qxy2Iwzso229zZa3/jS589aBD/Z83HThttXowYywMqjP68MNnzlb5thpQFkamzXfeziXflGf+mMiiHZHr2NPSOGamgCowqmqRK3Lmq0E1+CIAGzWOuRC0YhW41uu8A09AKScWWb10udPQhkolnwRZxOMXyRQmSAhCV0XrcaBv nope@nowheere.it
    - ssh-rsa AAAA.somestuffgoeshere home@home.com
- name: user3
  keys:
    - ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBBB17LMqtZmwdqXkrlwCMp9fcvRCvi1aU2jd64lmqjQWJoKBSRgRohRCKmcqtBM3nqFHNDnAC2ZBYkEXbRq1F2c= alessio@bradipo

...
```

Example Playbook
----------------

```yaml
- hosts: all
  roles:
    - role: ics-ans-role-localusers
```

License
-------

BSD 2-clause
