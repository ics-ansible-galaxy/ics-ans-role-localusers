import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_user(host):
    user1 = host.user("user1")
    user2 = host.user("user2")
    user3 = host.user("user3")
    assert user1.exists
    assert user2.exists
    assert user3.exists
